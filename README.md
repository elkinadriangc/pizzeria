![Pizzería](http://www.madarme.co/portada-web.png)
## Título del proyecto:

## Pizzería La QQTEÑA

### Índice
1. [Características](#-características)
2. [Contenido del proyecto](#-contenido-del-proyecto)
3. [Tecnologías](#-tecnologías-)
4. [IDE](#-ide)
5. [Instalación](#-instalación)
6. [Demo](#-demo)
7. [Autor](#-autor)
8. [Institución Académica](#-institución-académica)

## Características: 
- Proyecto con lectura de datos json a través de la API fecth JavaScript
- Carga dinámica del JSON
- Lectura de datos json a través de la API fecth JavaScript
- Archivo json de ejemplo: [ver](https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json)


## Contenido del proyecto:
- **index.html:** Archivo principal de la página web donde se ingresa la cantidad de pizzas y posteriormente se envía a opciones.html.

- **opciones.html:** Se recibe la cantidad de pizzas y tamaños enviados desde la página principal index.html, se selecccionan los sabores de la pizza y se envía a factura.html.

- **factura.html** Acá se recibe la información que es enviada por opciones.html como tamaño, cantida, y adicionales, se utiliza la información obtenida por el URL.

- **js/pizzeria.js** Acá se encuentra toda la lógica del programa donde están las funciones como la de leer los datos de JSON.



## Tecnologías :

- *HTML 5*
- *JavaScript*
- *Bootstrap* 
- *CSS* 


## IDE:

El proyecto se desarrolla usando sublime Text 3


## Instalación:

Se necesita de un navegador web para poder utilizar la página, para comenzar a utilizar la página de debe invocar al index.html.


## Demo:
Para ver el demo de la aplicación puede dirigirse a: [Pizzeria](http://ufps7.madarme.co/pizzeria/).



## Autor:
Proyecto desarrollado por Elkin Granados
Código: 1151658.

## Institución Académica:  
Proyecto desarrollado para la Materia programación web grupo B del Programa de Ingeniería de Sistemas de la Universidad Francisco de Paula Santander.

